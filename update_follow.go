package main

import (
	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/cloud/datastore"

	z "bitbucket.org/zup/types"
)

const (
	projectID = "prod-lkl-0"
	//pathJson  = "/home/xin/my_prog_go/src/file/prod-lkl.json"

	//projectID = "exp-lkl"
	//pathJson  = "/home/xin/my_prog_go/src/file/exp-lkl.json"

	limitMulti = 200
)

func main() {
	ctx := context.Background()
	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		fmt.Printf("Error NewClient: %+v \n", err)
		return
	}

	over := false
	follows := make([]*z.Follow, 0, limitMulti)
	q := datastore.NewQuery(z.DatastoreTableNames.Follow).Limit(limitMulti)
	it := client.Run(ctx, q) //GetAll(ctx, q, &follows)

	keysFollowing := make([]*datastore.Key, 0, 500)
	keysFollower := make([]*datastore.Key, 0, 500)

	followingCount := make(map[string]int32)
	followerCount := make(map[string]int32)

	for !over {
		first := true
		for {
			f := &z.Follow{}
			_, err = it.Next(f)
			if err != nil {
				if err == datastore.Done {
					if first {
						over = true
					}
					break
				}
				fmt.Printf("Error it.Next: %+v \n", err)
				continue
			}
			first = false
			follows = append(follows, f)
		}

		if len(follows) > 0 {
			for _, follow := range follows {
				src := follow.IdMemberFrom.String()
				dst := follow.IdProfileTo.String()

				if nb, exist := followingCount[src]; exist {
					followingCount[src] = nb + 1
				} else {
					followingCount[src] = 1
					keysFollowing = append(keysFollowing, datastore.NewKey(ctx, z.DatastoreTableNames.Profile, src, 0, nil))
				}

				if nb, exist := followerCount[dst]; exist {
					followerCount[dst] = nb + 1
				} else {
					followerCount[dst] = 1
					keysFollower = append(keysFollower, datastore.NewKey(ctx, z.DatastoreTableNames.Profile, dst, 0, nil))
				}
			}
		}

		follows = follows[:0]

		token, err := it.Cursor()
		if err != nil {
			fmt.Printf("Error Cursor(): %+v \n", err)
		}
		q = q.Start(token)
		it = client.Run(ctx, q)
	}

	//fmt.Println("followerCount: ", len(followerCount), len(keysFollower))
	//fmt.Println("followingCount: ", len(followingCount), len(keysFollowing))

	//fmt.Printf("My follower: %s \n", followerCount["0aG4n8(m)2vw"])
	//fmt.Printf("My following: %s \n", followingCount["0aG4n8(m)2vw"])

	if len(followerCount) > 499 || len(followingCount) > 499 {
		fmt.Printf("Multi can run for <500: followerCount(%d) followingCount(%d) \n", len(followerCount), len(followingCount))
		//return
	}

	start := 0
	end := 499
	if len(followingCount) < 499 {
		end = len(followingCount)
	}
	profilesFollowingRes := make([]*z.Profile, 0, 499)
	keysFollowingRes := make([]*datastore.Key, 0, 499)
	for {
		// get following profile
		profilesFollowing := make([]z.Profile, (end - start))
		if err := client.GetMulti(ctx, keysFollowing[start:end], profilesFollowing); err != nil {
			fmt.Printf("Error GetMulti profile following (%d-%d): %+v \n", start, end, err)
			//return
		}

		// update following profile
		for i, profile := range profilesFollowing {
			// NoSuchEntity
			if profile.Id.String() == "" {
				//fmt.Printf("Key (%+v) Vide! \n", keysFollowing[i])
				//client.Delete(ctx, keysFollowing[i])
				continue
			}
			// BUG!
			// if profile.Followers < 0 || profile.Followings < 0 {
			// 	fmt.Printf("Profile: %+v \n", profile)
			// }

			if _, exist := followerCount[profile.Id.String()]; !exist {
				profilesFollowing[i].Followers = 0
			}

			profilesFollowing[i].Followings = followingCount[profile.Id.String()]
			profilesFollowingRes = append(profilesFollowingRes, &profilesFollowing[i])
			keysFollowingRes = append(keysFollowingRes, keysFollowing[i])
		}
		//put following profile
		if _, err := client.PutMulti(ctx, keysFollowingRes, profilesFollowingRes); err != nil {
			fmt.Printf("Error PutMulti profile following: %+v \n", err)
		}
		fmt.Printf("%d Followings put\n", len(profilesFollowingRes))

		start = start + 499

		if end == len(followingCount) { // Last?
			break
		}

		if end+499 > len(followingCount) {
			end = len(followingCount)
		} else {
			end = end + 499
		}
	}

	start = 0
	end = 499
	if len(followerCount) < 499 {
		end = len(followerCount)
	}
	profilesFollowerRes := make([]*z.Profile, 0, 499)
	keysFollowerRes := make([]*datastore.Key, 0, 499)
	for {
		// get following profile
		profilesFollower := make([]z.Profile, (end - start))
		if err := client.GetMulti(ctx, keysFollower[start:end], profilesFollower); err != nil {
			fmt.Printf("Error GetMulti profile follower (%d-%d): %+v \n", start, end, err)
			//return
		}

		// update following profile
		for i, profile := range profilesFollower {
			// NoSuchEntity
			if profile.Id.String() == "" {
				//fmt.Printf("Key (%+v) Vide! \n", keysFollower[i])
				//client.Delete(ctx, keysFollower[i])
				continue
			}
			// BUG!
			// if profile.Followers < 0 || profile.Followings < 0 {
			// 	fmt.Printf("Profile: %+v \n", profile)
			// }

			if _, exist := followingCount[profile.Id.String()]; !exist {
				profilesFollower[i].Followings = 0
			}

			profilesFollower[i].Followers = followerCount[profile.Id.String()]
			profilesFollowerRes = append(profilesFollowerRes, &profilesFollower[i])
			keysFollowerRes = append(keysFollowerRes, keysFollower[i])
		}
		//put following profile
		if _, err := client.PutMulti(ctx, keysFollowerRes, profilesFollowerRes); err != nil {
			fmt.Printf("Error PutMulti profile follower: %+v \n", err)
		}
		fmt.Printf("%d Followers put\n", len(profilesFollowerRes))
		keysFollowerRes = keysFollowerRes[:0]
		profilesFollowerRes = profilesFollowerRes[:0]

		start = start + 499

		if end == len(followerCount) { // Last?
			break
		}

		if end+499 > len(followerCount) {
			end = len(followerCount)
		} else {
			end = end + 499
		}
	}

	// for i, v := range profilesFollowerRes {
	// 	fmt.Printf("%d Follower: %+v\n", i, v)
	// }

	fmt.Println("ok")
}
