package main

import (
	"fmt"

	z "bitbucket.org/zup/types"

	"golang.org/x/net/context"
	"google.golang.org/cloud/datastore"
)

const (
	projectID = "exp-lkl" //"prod-lkl-0"
	//pathJson  = "/home/xin/my_prog_go/src/file/exp-lkl.json"
)

func main() {
	ctx := context.Background()
	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		fmt.Printf("Error NewClient: %+v \n", err)
		return
	}

	postsDinstinct := make(map[string]bool)
	postsLike := make(map[string]int)
	postsComment := make(map[string]int)

	toUpdatePosts := make([]z.Post, 0, 499)
	toUpdateKeys := make([]*datastore.Key, 0, 499)

	// Get all like action
	q := datastore.NewQuery(z.DatastoreTableNames.Action).Filter("Type=", z.ActionTypeLike)
	it := client.Run(ctx, q)
	for {
		actionLike := &z.Action{}
		_, err := it.Next(actionLike)
		if err != nil {
			if err == datastore.Done {
				break
			}

			fmt.Printf("Error Next like: %+v \n", err)
			continue
		}

		// check likes count
		if _, exist := postsLike[actionLike.Post.Id.String()]; !exist {
			postsDinstinct[actionLike.Post.Id.String()] = true
			postsLike[actionLike.Post.Id.String()] = 1
		} else {
			postsLike[actionLike.Post.Id.String()] = postsLike[actionLike.Post.Id.String()] + 1
		}
	}

	// Get all comment action
	q = datastore.NewQuery(z.DatastoreTableNames.Action).Filter("Type=", z.ActionTypePostComment)
	it = client.Run(ctx, q)

	for {
		actionComment := &z.Action{}
		_, err := it.Next(actionComment)
		if err != nil {
			if err == datastore.Done {
				break
			}

			fmt.Printf("Error Next comment: %+v \n", err)
			continue
		}

		// check comments count
		if _, exist := postsComment[actionComment.Post.Id.String()]; !exist {
			postsComment[actionComment.Post.Id.String()] = 1
		} else {
			postsComment[actionComment.Post.Id.String()] = postsComment[actionComment.Post.Id.String()] + 1
		}

		if exist, _ := postsDinstinct[actionComment.Post.Id.String()]; !exist {
			postsDinstinct[actionComment.Post.Id.String()] = true
		}
	}

	//fmt.Println(len(postsLike))
	//fmt.Println(len(postsComment))
	//fmt.Println(len(postsDinstinct))

	// compare the post by multi (datastore supports max 500)
	keys := make([]*datastore.Key, 0, 499)
	posts := make([]z.Post, 499)
	for postID, _ := range postsDinstinct {
		keys = append(keys, datastore.NewKey(ctx, z.DatastoreTableNames.Post, postID, 0, nil))

		if len(keys) == 499 { // max num to getmulti
			//fmt.Println(len(keys), len(posts))
			err = client.GetMulti(ctx, keys, posts)
			if err != nil {
				fmt.Printf("Error GetMulti all posts dinstinct: %+v \n", err)
				//return
			}

			for i, post := range posts {
				if post.Id == z.ID("") {
					fmt.Printf("Key err: %+v \n", keys[i])
					continue
				}

				// post.likes
				if nb, exist := postsLike[post.Id.String()]; exist {
					posts[i].Likes = int64(nb)
				}
				// post.comments
				if nb, exist := postsComment[post.Id.String()]; exist {
					posts[i].Comments = int64(nb)
				}
				// the posts have bad count
				if post.Likes != posts[i].Likes || post.Comments != posts[i].Comments {
					toUpdatePosts = append(toUpdatePosts, posts[i])
					toUpdateKeys = append(toUpdateKeys, keys[i])
					//fmt.Printf("%s likes (%d-%d) comments (%d-%d) \n", post.Id.String(), post.Likes, posts[i].Likes, post.Comments, posts[i].Comments)
				}
			}

			keys = keys[:0]
		}
	}

	// the left posts (less than 499)
	if len(keys) > 0 {
		posts := make([]z.Post, len(keys))

		err = client.GetMulti(ctx, keys, posts)
		if err != nil {
			fmt.Printf("Error GetMulti the left post dinstinct: %+v \n", err)
			//return
		}

		for i, post := range posts {
			// post.likes
			if nb, exist := postsLike[post.Id.String()]; exist {
				post.Likes = int64(nb)
			}
			// post.comments
			if nb, exist := postsComment[post.Id.String()]; exist {
				post.Comments = int64(nb)
			}
			// the posts have bad count
			if post.Likes != posts[i].Likes || post.Comments != posts[i].Comments {
				toUpdatePosts = append(toUpdatePosts, posts[i])
				toUpdateKeys = append(toUpdateKeys, keys[i])
				//fmt.Printf("%s likes (%d-%d) comments (%d-%d) \n", post.Id.String(), post.Likes, posts[i].Likes, post.Comments, posts[i].Comments)
			}

		}
	}

	//fmt.Println(len(toUpdatePosts), len(toUpdateKeys))

	// DELETE WHEN OK
	return

	// Put the Posts: update count
	_, err = client.PutMulti(ctx, toUpdateKeys, toUpdatePosts)
	if err != nil {
		fmt.Printf("Error PutMulti the toUpdatePosts: %+v \n", err)
	}

	fmt.Println("ok")
}
