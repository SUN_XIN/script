package main

import (
	"fmt"
	"strings"

	z "bitbucket.org/zup/types"

	"golang.org/x/net/context"
	"google.golang.org/cloud/datastore"
)

const (
	projectID = "prod-lkl-0" //"exp-lkl"

	RiveID = "0aGK6x(m)JNI" // "0b1RvP(m)PWy"
)

func main() {
	ctx := context.Background()
	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		fmt.Printf("Error NewClient: %+v \n", err)
		return
	}

	keysProfile := make([]*datastore.Key, 0, 499)
	keysFollows := make([]*datastore.Key, 0, 499)
	newFollows := make([]*z.Follow, 0, 499)

	// all member
	q := datastore.NewQuery(z.DatastoreTableNames.Member)
	it := client.Run(ctx, q)
	for {
		member := &z.Member{}
		_, err := it.Next(member)
		if err != nil {
			if err == datastore.Done {
				break
			}

			fmt.Printf("Error Next member: %+v \n", err)
			continue
		}

		//* For each member ...

		idStr := member.Id.String()

		if idStr == RiveID {
			continue
		}

		//Get all follow
		qFollow := datastore.NewQuery(z.DatastoreTableNames.Follow).Filter("IdMemberFrom=", idStr)
		follows := []*z.Follow{}
		_, err = client.GetAll(ctx, qFollow, &follows)
		if err != nil {
			fmt.Printf("Error GetAll Follow(%s): %+v \n", idStr, err)
			continue
		}

		// already follow?
		exist := false
		for _, f := range follows {
			if f.IdProfileTo.String() == RiveID {
				exist = true
				break
			}
		}

		// not yet follow
		if !exist {
			f := z.NewFollow(member.Id, z.ID(RiveID))
			newFollows = append(newFollows, f)
			keysFollows = append(keysFollows, datastore.NewKey(ctx, z.DatastoreTableNames.Follow, f.DatastoreIDString(), 0, nil))
			keysProfile = append(keysProfile, datastore.NewKey(ctx, z.DatastoreTableNames.Profile, idStr, 0, nil))
			//fmt.Println(idStr)
		}
	}

	if len(keysProfile) > 500 || len(keysFollows) > 500 || len(newFollows) > 500 {
		fmt.Printf("> 500: %d %d %d \n", len(keysProfile), len(keysFollows), len(newFollows))
		return
	}

	// get the profiles to prepare update profile.followings
	profiles := make([]z.Profile, len(keysProfile))
	err = client.GetMulti(ctx, keysProfile, profiles)
	if err != nil {
		fmt.Printf("Error GetMulti profiles: %+v \n", err)
		return
	}

	// check and update profile.follow
	profilesPut := make([]*z.Profile, 0, len(keysProfile))
	profilesKeysPut := make([]*datastore.Key, 0, len(keysProfile))
	for i, profile := range profiles {
		if !strings.Contains(profile.Id.String(), "(m)") && profile.Followings != 0 {
			fmt.Printf("Profile (%s) can not have following\n", profile.Id.String())
			continue
		}

		if profile.Id.String() == "" {
			fmt.Printf("Profile vide \n")
			continue
		}
		if profile.Followers < 0 || profile.Followings < 0 {
			fmt.Printf("Profile (%s) follow < 0 \n", profile.Id.String())
			continue
		}
		//fmt.Printf("src %d, dst %d \n", profiles[i].Followings, profiles[i].Followers)
		profiles[i].Followings = profiles[i].Followings + 1
		profilesPut = append(profilesPut, &profiles[i])
		profilesKeysPut = append(profilesKeysPut, keysProfile[i])
	}

	// DELETE WHEN OK
	return

	// put profiles
	_, err = client.PutMulti(ctx, profilesKeysPut, profilesPut)
	if err != nil {
		fmt.Printf("Error PutMulti profiles: %+v \n", err)
		return
	}

	// put follows
	_, err = client.PutMulti(ctx, keysFollows, newFollows)
	if err != nil {
		fmt.Printf("Error PutMulti follows: %+v \n", err)
		return
	}
}

